import Vue from "vue";
import Vuex from "vuex";
import _ from "lodash";

Vue.use(Vuex);

// Не знаю можно ли было юзать store2 для работы с локалСторейджем
// Поэтому я написал всё своими руками

export default new Vuex.Store({
  state: {
    users: localStorage.users ? JSON.parse(localStorage.users) : []
  },
  mutations: {
    addUser(state, users) {
      state.users = users;
    },
    resetUsers(state) {
      state.users = [];
    },
    deleteUser(state, users) {
      state.users = users;
    },
    editUser(state, users) {
      state.users = users;
    }
  },
  actions: {
    addUser({ commit }, user) {
      let usersLocal = localStorage.users;
      let usersNew;

      usersNew = usersLocal ? _.union(JSON.parse(usersLocal), [user]) : [user];
      localStorage.setItem("users", JSON.stringify(usersNew));
      commit("addUser", usersNew);
    },
    resetUsers({ commit }) {
      let usersLocal = localStorage.users;

      if (usersLocal) {
        localStorage.removeItem("users");
      }
      commit("resetUsers");
    },
    deleteUser({ commit }, user) {
      let usersLocal = localStorage.users;

      if (usersLocal) {
        usersLocal = JSON.parse(usersLocal);
        _.remove(usersLocal, function(e) {
          return e.id === user.id;
        });
        localStorage.setItem("users", JSON.stringify(usersLocal));
        commit("deleteUser", usersLocal);
      }
    },
    editUser({ commit }, user) {
      let usersLocal = localStorage.users;

      if (usersLocal) {
        usersLocal = JSON.parse(usersLocal);
        let newUsersLocal = _.map(usersLocal, o => {
          return o.id === user.id ? user : o;
        });

        localStorage.setItem("users", JSON.stringify(newUsersLocal));
        commit("editUser", newUsersLocal);
      }
    }
  },
  modules: {}
});
